package test;

/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */

import model.PlayList;
import model.Song;

public class QueueAndPlay3SongsInOrder
{

	public static String baseDir = System.getProperty("user.dir")
			+ System.getProperty("file.separator") + "songfiles"
			+ System.getProperty("file.separator");

	public static void main(String[] args)
	{

		// Assign the responsibility of queuing Songs and playing them in order,
		// and not overlapping
		javax.swing.SwingUtilities.invokeLater(new Runnable()
		{
			public void run()
			{
				PlayList playList = new PlayList();

				Song a = (new Song("Space Music", 7, "Sun Microsytems", baseDir + "spacemusic.au"));
				Song b = (new Song("Flute", 7, "Sun Microsytems", baseDir + "flute.aif"));
				Song c = (new Song("Blue Ridge Mountain Mist", 39, "Schuckett, Ralph", baseDir + "BlueRidgeMountainMist.mp3"));

				// Play 3 songs in FIFO order
				playList.queueUpNextSong(a);
				playList.queueUpNextSong(b);
				playList.queueUpNextSong(c);
			}
			
		});
	}
}


/*
import java.io.File;

import model.Playlist;
import model.Song;

public class QueueAndPlay3SongsInOrder
{

	static String sep = File.separator;

	public static void main(String[] args)
	{
	Playlist play = new Playlist();
	play.enqueue(new Song("songfiles" + sep + "tada.wav", 0));
	play.enqueue(new Song("songfiles" + sep + "flute.aif", 0));
	play.enqueue(new Song("songfiles" + sep + "UntameableFire.mp3", 0));

	}

}
*/