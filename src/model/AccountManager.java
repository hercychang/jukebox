package model;
/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */
import java.util.HashMap;

/*
 * Contains a collection of user accounts; given a username and password, it can return a user object, provided the username designates an actual
 * user and the password provided is correct. It returns null otherwise.
 */

public class AccountManager
{
	private HashMap<String, Account> accounts;
	
	public AccountManager()
	{
		accounts = new HashMap<String, Account>();
	}
	
	public void add(Account account)
	{
		accounts.put(account.getName(), account);
	}

	public Account retrieveAccount(String name, String password)
	{
		Account findedAccount = accounts.get(name);
		
		if(findedAccount != null && findedAccount.checkPassword(password))
		{
			return findedAccount;
		}
		return null;
	}
	
	
}
