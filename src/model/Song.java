package model;
/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/*
 * Represents a particular song available for selection; it provides the filename of the song file, and tracks how many times
 * in a given day it has been played, responding to queries over whether it can still be played in that day
 */

public class Song
{

	private String fileName;
	private String trackName;
	private String artistName;
	private int songLengthSec;
	
	private int timesPlayed;
	private GregorianCalendar refDate;

	public Song(String fileName, int songLengthSec)
	{
		this.fileName = fileName;
		this.songLengthSec = songLengthSec;
		this.refDate = new GregorianCalendar();
		
		this.timesPlayed = 0;
	}
	
	public Song(String trackName, int songLengthSec, String artistName, String fileName)
	{
		this.fileName = fileName;
		this.trackName = trackName;
		this.artistName = artistName;
		this.songLengthSec = songLengthSec;
		
		this.timesPlayed = 0;
		this.refDate = new GregorianCalendar();
	}
	
	
	public boolean canBePlayed()
	{
		return timesPlayed < 5;
	}

	public int getSongLengthSec()
	{
		return songLengthSec;
	}

	public void increaseCount()
	{
		this.verifyDate();
		this.timesPlayed++;
	}
	
	public String getFileName()
	{
		return fileName;
	}
	
	public String getTrackName()
	{
		return trackName;
	}
	
	public String getArtistName()
	{
		return artistName;
	}
	
	private void verifyDate()
	{
		GregorianCalendar realtoday = new GregorianCalendar();
		if(this.refDate.get(Calendar.YEAR) != realtoday.get(Calendar.YEAR) ||
				this.refDate.get(Calendar.MONTH) != realtoday.get(Calendar.MONTH) ||
				this.refDate.get(Calendar.DATE) != realtoday.get(Calendar.DATE))
		{
			this.refDate = realtoday;
			this.timesPlayed = 0;
		}
	}

	
	public String toString()
	{
		SimpleDateFormat minFormat = new SimpleDateFormat("mm:ss");
		return "(" + minFormat.format(songLengthSec * 1000) + ") " + trackName;// + " -by " + artistName ;
	}

	public int getTimesPlayed()
	{
		return timesPlayed;
	}

}
