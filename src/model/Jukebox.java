package model;



/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */

import java.io.File;
/*
 * Links the various components of a user authenticating, song selecting, song
 * playing system
 */
public class Jukebox
{
	private final int MAX_USER_SECS = 1500 * 60;
	private final PlayList playlist;
	private AccountManager manager;
	private SongCollection songs;
	private Account currentUser;

	public Jukebox()
	{
		this.playlist = new PlayList();

		this.manager = new AccountManager();
		this.manager.add(new Account("Ali", "1111"));
		this.manager.add(new Account("Chris", "2222"));
		this.manager.add(new Account("River", "3333"));
		this.manager.add(new Account("Ryan", "4444"));
		this.manager.add(new Account("Hercy", "946295w78k"));
		this.songs = new SongCollection();
		/*
		 * songs.add(new Song("songfiles" + File.separator +
		 * "BlueRidgeMountainMist.mp3", 0)); songs.add(new Song("songfiles" +
		 * File.separator + "DeterminedTumbao.mp3", 0)); songs.add(new
		 * Song("songfiles" + File.separator + "flute.aif", 0)); songs.add(new
		 * Song("songfiles" + File.separator + "spacemusic.au", 0));
		 * songs.add(new Song("songfiles" + File.separator + "SwingCheese.mp3",
		 * 0)); songs.add(new Song("songfiles" + File.separator + "tada.wav",
		 * 0)); songs.add(new Song("songfiles" + File.separator +
		 * "UntameableFire.mp3", 0));
		 */

		songs.add(new Song("Blue Ridge Mountain Mist", 38, "Ralph Schuckett",
				"songfiles" + File.separator + "BlueRidgeMountainMist.mp3"));

		songs.add(new Song("Determined Tumbao", 20, "FreePlay Music",
				"songfiles" + File.separator + "DeterminedTumbao.mp3"));

		songs.add(new Song("Flute", 5, "Sun Microsystems", "songfiles"
				+ File.separator + "flute.aif"));

		songs.add(new Song("Space Music", 6, "Unknown", "songfiles"
				+ File.separator + "spacemusic.au"));

		songs.add(new Song("Swing Cheese", 15, "FreePlay Music", "songfiles"
				+ File.separator + "SwingCheese.mp3"));

		songs.add(new Song("Tada", 2, "Microsoft", "songfiles" + File.separator
				+ "tada.wav"));

		songs.add(new Song("Untameable Fire", 282, "Pierre Langer", "songfiles"
				+ File.separator + "UntameableFire.mp3"));
		
		songs.add(new Song("We Can Work It Out", 136, "The Beatles",
				"songfiles" + File.separator + "WeCanWorkItOut.mp3"));
		
		songs.add(new Song("Whip It", 160, "Devo",
				"songfiles" + File.separator + "WhipIt.mp3"));
		

		currentUser = null;

	}

	public boolean login(String username, String password)
	{
		currentUser = manager.retrieveAccount(username, password);

		return currentUser != null;
	}

	public void logout()
	{
		currentUser = null;
	}

	public SongCollection getSongs()
	{
		return songs;
	}

	public Account getCurrentUser()
	{
		return currentUser;
	}
	
	public PlayList getPlaylist()
	{
		return playlist;
	}

	public boolean playSong(Song song)
	{
		if(currentUser == null)
		{
			return false;
		}
		if(currentUser.canPlay(song, MAX_USER_SECS))
		{
			playlist.queueUpNextSong(song);
			return true;
		}
		else
		{
			return false;
		}
	}

	public int getMaxPlayTime()
	{
		return MAX_USER_SECS;
	}
}
