package model;
/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */
import java.util.Calendar;
import java.util.GregorianCalendar;

/*
 * Represents a single user's information in a jukebox system, containing information for user authentification, and for verifying whether
 * a user may play a particular song, based on how many times the user has played a song in a day, how many times the song has been played in the day,
 * and whether the user has sufficient minutes remaining
 */

public class Account
{
	private String name;
	private String password;
	private int songsPlayedToday,
				secsPlayed;
	private GregorianCalendar refDate;
	
	public Account(String name, String password)
	{
		this.name = name;
		this.password = password;
		this.songsPlayedToday = 0;
		this.secsPlayed = 0;
		this.refDate = new GregorianCalendar();
	}
	
	public boolean checkPassword(String typeIn)
	{
		return typeIn.equals(password);
	}
	
	
	private void verifyDate()
	{
		GregorianCalendar realtoday = new GregorianCalendar();
		if(this.refDate.get(Calendar.YEAR) != realtoday.get(Calendar.YEAR) ||
				this.refDate.get(Calendar.MONTH) != realtoday.get(Calendar.MONTH) ||
				this.refDate.get(Calendar.DATE) != realtoday.get(Calendar.DATE))
		{
			this.refDate = realtoday;
			this.songsPlayedToday = 0;
		}
	}
	
	public boolean canPlay(Song song, int maxSec)
	{
		this.verifyDate();
		return song.canBePlayed() && maxSec >= secsPlayed + song.getSongLengthSec() && songsPlayedToday < 2;
	}
	
	//Does not and cannot check if it's allowed to play the song; Jukebox must check before calling
	public void playSong(Song song, PlayList playlist)
	{
		playlist.queueUpNextSong(song);
		this.songsPlayedToday++;
		this.secsPlayed += song.getSongLengthSec();
		song.increaseCount();
	}

	public String getName()
	{
		return name;
	}
	
	public void increaseCountAndSecsPlayed(int secs)
	{
		this.verifyDate();
		this.songsPlayedToday++;
		this.secsPlayed = this.secsPlayed + secs;
	}
	
	public String getSongsLeft()
	{
		int temp = 2 - songsPlayedToday;
		
		return "" + temp;
	}

	public int getTimeLeft(int maxSec)
	{		
		return maxSec - secsPlayed;
	}

}
