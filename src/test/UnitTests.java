package test;
/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */

import static org.junit.Assert.*;

import java.io.File;
import java.util.GregorianCalendar;

import model.*;



import org.junit.Test;

import songplayer.EndOfSongEvent;

//import songplayer.EndOfSongEvent;

public class UnitTests {

	@Test
	public void testFreshSongPlayable() 
	{
		Song song = new Song("songfiles" + File.separator + "tada.wav", 0);
		assertTrue(song.canBePlayed());
	}
	
	@Test
	public void testFiveIncrementsToSongUnplayable()
	{
		Song song = new Song("songfiles" + File.separator + "tada.wav", 0);
		for (int i = 0; i < 5; i++) song.increaseCount();
		assertFalse(song.canBePlayed());
	}
	
	@Test
	public void testSongGetters()
	{
		Song song = new Song("songfiles" + File.separator + "tada.wav", 0);
		assertEquals(song.getSongLengthSec(), 0);
		assertEquals(song.getFileName(), "songfiles" + File.separator + "tada.wav");
	}
	
	@Test
	public void testCanRetrieveSongFromCollection()
	{
		Song song = new Song("songfiles" + File.separator + "tada.wav", 0);
		SongCollection songs = new SongCollection();
		songs.add(song);
		assertTrue(songs.getList().contains(song));
	}
	
	@Test
	public void testSongCollectionCannotBeAbridged()
	{
		Song song = new Song("songfiles" + File.separator + "tada.wav", 0);
		SongCollection songs = new SongCollection();
		songs.add(song);
		songs.getList().remove(song);
		assertTrue(songs.getList().contains(song));
	}
	
	@Test
	public void testWrongPassword()
	{
		Account user = new Account("Bill", "letmein");
		assertFalse(user.checkPassword("swordfish"));
	}
	
	@Test
	public void testRightPassword()
	{
		Account user = new Account("Bill", "letmein");
		assertTrue(user.checkPassword("letmein"));
	}
	
	@Test
	public void testCanPlayFreshSongFreshUser()
	{
		Song song = new Song("songfiles" + File.separator + "tada.wav", 0);
		Account user = new Account("Bill", "letmein");
		
		
		assertTrue(user.canPlay(song, 1400));
		
	}
	
	@Test
	public void testCannotPlayExpiredSongFreshUser()
	{
		
		Song song = new Song("songfiles" + File.separator + "tada.wav", 0);
		Account user = new Account("Bill", "letmein");
		for (int i = 0; i < 5; i++) song.increaseCount();
		
		assertFalse(user.canPlay(song, 1400));
		
		
	}
	
	@Test
	public void testAccountGetter()
	{
		Account user = new Account("Bill", "letmein");
		
		assertEquals(user.getName(), "Bill");
	}
	
	
	@Test
	public void testGetProperAccount()
	{
		Account user = new Account("Bill", "letmein");
		AccountManager users = new AccountManager();
		users.add(user);
		
		assertEquals(users.retrieveAccount("Bill", "letmein"), user);
		
	}
	
	@Test
	public void testGetWrongUsername()
	{
		
		Account user = new Account("Bill", "letmein");
		AccountManager users = new AccountManager();
		users.add(user);
		
		assertEquals(users.retrieveAccount("Bil", "letmein"), null);
		
		
		
		
	}
	
	@Test
	public void testGetWrongPassword()
	{
		Account user = new Account("Bill", "letmein");
		AccountManager users = new AccountManager();
		users.add(user);
		
		assertEquals(users.retrieveAccount("Bill", "swordfish"), null);
		
	}
	
	@Test
	public void testEmptyAccountManager()
	{
		
		AccountManager users = new AccountManager();
		
		
		assertEquals(users.retrieveAccount("Bill", "letmein"), null);
		
	}
	
	@Test
	public void testPlayASong()
	{
		Song song = new Song("songfiles" + File.separator + "tada.wav", 0);
		Account user = new Account("Bill", "letmein");
		
		user.playSong(song, new PlayList());
		assertTrue(user.canPlay(song, 1400));
		assertTrue(song.canBePlayed());
	}
	
	@Test
	public void testQueueSongs()
	{
		PlayList list = new PlayList();
		Song song = new Song("songfiles" + File.separator + "tada.wav", 0);
		list.queueUpNextSong(song);
		list.queueUpNextSong(song);
		list.queueUpNextSong(song);
	}
	
	
	@Test
	public void testJukeboxCanLogin()
	{
		Jukebox box = new Jukebox();
		assertTrue(box.login("Ali", "1111"));
		
	}
	
	@Test
	public void testJukeboxFailedLogin()
	{
		Jukebox box = new Jukebox();
		assertFalse(box.login("Alli", "1111"));
	}
	
	@Test
	public void testJukeboxUserAccessible()
	{
		Jukebox box = new Jukebox();
		box.login("Ali", "1111");
		assertEquals(box.getCurrentUser().getName(), "Ali");
	}
	
	@Test
	public void testJukeboxLogout()
	{
		
		Jukebox box = new Jukebox();
		box.login("Ali", "1111");
		box.logout();
		assertEquals(box.getCurrentUser(), null);
		
	}
	
	@Test
	public void testGetCorrectSongList()
	{
		Jukebox box = new Jukebox();
		box.login("Ali", "1111");
		assertEquals(box.getSongs().getList().size(), 7);
	}
	
	@Test
	public void testPlaySong()
	{
		Jukebox box = new Jukebox();
		box.login("Ali", "1111");
		assertTrue(box.playSong(new Song("songfiles" + File.separator + "BlueRidgeMountainMist.mp3", 0)));
	}
	
	@Test
	public void testPlayListFinishedPlaying()
	{
		PlayList playList = new PlayList();

		// test when empty
		playList.songFinishedPlaying(new EndOfSongEvent("songfiles/spacemusic.au", new GregorianCalendar()));
		playList.songFinishedPlaying(null);
		
		// test when queued up two songs
		playList.queueUpNextSong(new Song("songfiles/flute.aif", 7));
		//playList.songFinishedPlaying(new EndOfSongEvent("songfiles/flute.aif", new GregorianCalendar()));
		playList.queueUpNextSong(new Song("Blue Ridge Mountain Mist", 39, "Schuckett, Ralph", "songfiles/BlueRidgeMountainMist.mp3"));
		playList.songFinishedPlaying(new EndOfSongEvent("songfiles/BlueRidgeMountainMist.mp3", new GregorianCalendar()));
		//playList.songFinishedPlaying(null);
		

	}
	
	@Test public void testGettersSong()
	{
		Song song = new Song("Blue Ridge Mountain Mist", 39, "Schuckett, Ralph", "songfiles/BlueRidgeMountainMist.mp3");
		
		assertEquals(song.getArtistName(), "Schuckett, Ralph");
		assertEquals(song.getTrackName(), "Blue Ridge Mountain Mist");
		song.toString();
	}
	
	@Test public void testAccountGets()
	{
		Account user = new Account("Ali","1111");
		
		assertEquals(user.getSongsLeft(), "2");
		
		assertEquals(user.getTimeLeft(1500*60), "18:00:00");
		
		user.increaseCountAndSecsPlayed(0);
		
		assertEquals(user.getSongsLeft(), "1");
	}
	

}