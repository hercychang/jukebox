A Java Jukebox Player with account management system, playlist support and beautiful GUI design.

![Jukebox_screenshot.png](https://bitbucket.org/repo/bGekAz/images/2565722317-Jukebox_screenshot.png)