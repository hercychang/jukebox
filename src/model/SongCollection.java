package model;

/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/*
 * Contains a collection of songs, allowing additions and provides a view of the songs in the collection.
 * May provide additional functionality in the future.
 */

public class SongCollection implements TableModel
{
	private List<Song> songList;

	// private List<TableModelListener> listeners;

	public SongCollection()
	{
		songList = new LinkedList<Song>();

		// On second thought, we probably don't need event listeners for this,
		// since it doesn't change
		// this.listeners = new ArrayList<TableModelListener>();
	}
	

	public void add(Song song)
	{
		songList.add(song);
	}

	public List<Song> getList()
	{
		return new LinkedList<Song>(songList);
	}

	/*
	 * Track Name |Time(mm:ss)| Artist Name | Plays Today|
	 */

	@Override
	public void addTableModelListener(TableModelListener arg0)
	{
		// Probably not needed, so operation unsupported

	}

	@Override
	public Class<?> getColumnClass(int arg0)
	{
		/*
		if(arg0 == 0 || arg0 == 1)
		{
			return String.class;
		}
		else if(arg0 == 2)
		{
			return int.class;
		}
		 */
		if(arg0 == 0 || arg0 == 1 || arg0 == 2)
		{
			return String.class;
		}
		else if(arg0 == 3)
		{
			return int.class;
		}
		else
			return null;
	}

	@Override
	public int getColumnCount()
	{

		return 4;
	}

	@Override
	public String getColumnName(int arg0)
	{
		switch(arg0)
		{
			case 0:
				return "Name";
			case 1:
				return "Time";
			case 2:
				return "Artist";
			case 3:
				return "Plays Today";
			default:
				return null;

		}
	}

	@Override
	public int getRowCount()
	{

		return this.songList.size();
	}

	@Override
	public Object getValueAt(int arg0, int arg1)
	{
		SimpleDateFormat minFormat = new SimpleDateFormat("mm:ss");
		switch(arg1)
		{
			case 0:
				return songList.get(arg0).getTrackName();
			case 1:
				return minFormat.format(songList.get(arg0).getSongLengthSec()*1000);
			case 2:
				return songList.get(arg0).getArtistName();
			case 3:
				return songList.get(arg0).getTimesPlayed();
			default:
				return null;
		}
	}
	

	@Override
	public boolean isCellEditable(int arg0, int arg1)
	{
		return false;
	}

	@Override
	public void removeTableModelListener(TableModelListener arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void setValueAt(Object arg0, int arg1, int arg2)
	{
		// Operation unsupported; do nothing

	}
	

}
