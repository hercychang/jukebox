package view;
/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.text.DecimalFormat;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import model.Jukebox;
import model.Song;
import model.SongCollection;


@SuppressWarnings("serial")
public class JukeboxGUI extends JFrame
{
	private Jukebox jukebox;
	private String versionInfo = "v3.4.1 (Beta)"; 
	private ImageIcon alertIcon = new ImageIcon("source/alert.png");
	private ImageIcon logInIcon = new ImageIcon("source/login.png");
	private JButton themeButton;
	private JLabel background; 
	private boolean isClassic;
	
	
	// Sign in and sign out
	private JTextField idField;
	private JPasswordField passwordField;
	private JButton signInButton;
	private boolean signedIn;
	
	// Collection table (Scrollable)
	private JTable songsCollectionTable;
	private TableModel songsCollectionTableModel;
	private JScrollPane songsCollectionTableScrollPane;
	
	// Playlist
	private JList<Song> playlistList;
	
	// Add to playlist button
	private JButton addButton;
	
	// User panel
	private JLabel username;
	private JLabel songsLeft;
	private JLabel timeLeft;
	private JLabel nameLabel;
	private JLabel songsLeftLabel;
	private JLabel timeLeftLabel;
	private JLabel usernameShadow;
	private JLabel songsLeftShadow;
	private JLabel timeLeftShadow;
	private JLabel nameLabelShadow;
	private JLabel songsLeftLabelShadow;
	private JLabel timeLeftLabelShadow;
	
	
	
	public static void main(String[] args)
	{
		JukeboxGUI UIView = new JukeboxGUI();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		UIView.setLocation(screenSize.width / 2 - UIView.getSize().width / 2, screenSize.height / 2 - UIView.getSize().height / 2);
		//UIView.setOpacity(10);
		//UIView.setBackground( new Color( 0.0f,0.0f,0.0f,0.3f));
	}
	
	public JukeboxGUI()
	{
		
		// Setup Program
		this.jukebox = new Jukebox();
		this.signedIn = false;

		
		// Setup Layout
		this.setLayout(null);
		
		
		
		/* Level 1 */
		/* Logo and Icon */
		JLabel icon = new JLabel(new ImageIcon("source/logo.png"));
		JLabel logo = new JLabel("Jukebox");
		JLabel logoShadow = new JLabel("Jukebox");
		
		// Setup Style
		logo.setForeground (Color.WHITE);
		logo.setFont( new Font("Gill Sans", Font.PLAIN, 24) );
		logoShadow.setFont( new Font("Gill Sans", Font.PLAIN, 24) );
		
		// Loaction & Size Setup
		icon.setBounds(0, 0, 80, 80);
		logo.setBounds(80, 0, 160, 80);
		logoShadow.setBounds(80, -1, 160, 80);
		
		// Add
		this.add(icon);
		this.add(logo);
		this.add(logoShadow);
		
		
		
		/* Level 2 */
		/* Sign in panel */
		idField = new JTextField();
		passwordField = new JPasswordField();
		signInButton = new JButton("Log In");
		JLabel usernameLabel = new JLabel("Username");
		JLabel passwordLabel = new JLabel("Password");
		JLabel usernameLabelShadow = new JLabel("Username");
		JLabel passwordLabelShadow = new JLabel("Password");
		
		// Setup Style
		usernameLabel.setForeground (Color.WHITE);
		passwordLabel.setForeground (Color.WHITE);
		usernameLabel.setFont( new Font("Helvetica", Font.PLAIN, 10) );
		passwordLabel.setFont( new Font("Helvetica", Font.PLAIN, 10) );
		usernameLabelShadow.setFont( new Font("Helvetica", Font.PLAIN, 10) );
		passwordLabelShadow.setFont( new Font("Helvetica", Font.PLAIN, 10) );
		idField.setBackground(Color.WHITE);
		passwordField.setBackground(Color.WHITE);
		
		// Setup ActionListener
		//idField.addActionListener(new signInAndOutListener());
		//passwordField.addActionListener(new signInAndOutListener());
		signInButton.addActionListener(new signInAndOutListener());
		
		// Loaction & Size Setup
		idField.setBounds(246, 28, 150, 24);
		passwordField.setBounds(411, 28, 150, 24);
		signInButton.setBounds(576, 28, 90, 24);
		usernameLabel.setBounds(250, 8, 150, 24);
		passwordLabel.setBounds(415, 8, 150, 24);
		usernameLabelShadow.setBounds(250, 7, 150, 24);
		passwordLabelShadow.setBounds(415, 7, 150, 24);
		
		// Add
		this.add(usernameLabel);
		this.add(passwordLabel);
		this.add(idField);
		this.add(passwordField);
		this.add(signInButton);
		this.add(usernameLabelShadow);
		this.add(passwordLabelShadow);
		

		/* Level 3 */
		/* Collection Table */
		songsCollectionTableModel = jukebox.getSongs();
		songsCollectionTable = new JTable(songsCollectionTableModel)
		{
		    public Component prepareRenderer(TableCellRenderer renderer, int row, int column)
		    {
		        Component returnComp = super.prepareRenderer(renderer, row, column);
		        Color alternateColor = Color.WHITE;
		        Color whiteColor = new Color(0xEEF2F5);
		        if(!returnComp.getBackground().equals(getSelectionBackground()))
		        {
		            Color rowColor = (row % 2 == 0 ? alternateColor : whiteColor);
		            returnComp.setBackground(rowColor);
		            rowColor = null;
		        }
		        
		        return returnComp;
		    }
		};
		songsCollectionTableScrollPane = new JScrollPane(songsCollectionTable);
		RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(songsCollectionTableModel);
		songsCollectionTable.setRowSorter(sorter);
		JLabel songCollectionLable = new JLabel("SONG COLLECTION");
		JLabel songCollectionLableShadow = new JLabel("SONG COLLECTION");
		
		// Setup Style
		songsCollectionTable.setFont( new Font("Helvetica", Font.PLAIN, 12) );
		songCollectionLable.setForeground (Color.WHITE);
		songCollectionLable.setHorizontalAlignment(SwingConstants.CENTER);
		songCollectionLable.setFont( new Font("Helvetica", Font.PLAIN, 10) );
		songCollectionLableShadow.setHorizontalAlignment(SwingConstants.CENTER);
		songCollectionLableShadow.setFont( new Font("Helvetica", Font.PLAIN, 10) );
		songsCollectionTable.setShowGrid(false);
		songsCollectionTable.setShowVerticalLines(false);
		songsCollectionTable.setShowHorizontalLines(false);
		songsCollectionTable.setIntercellSpacing(new Dimension(0, 0));
		songsCollectionTableScrollPane.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
		songsCollectionTable.getColumnModel().getColumn(0).setPreferredWidth(150);
		songsCollectionTable.getColumnModel().getColumn(1).setPreferredWidth(40);
		songsCollectionTable.getColumnModel().getColumn(2).setPreferredWidth(100);
		songsCollectionTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		songsCollectionTable.setRowHeight( 20 );
		songsCollectionTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		songsCollectionTable.setFocusable(false);
		songsCollectionTable.setRowSelectionAllowed(true);
		songsCollectionTable.setSelectionBackground(new Color(0x7997BC));
		
		// Location & Size Setup
		songsCollectionTableScrollPane.setBounds(251, 104, 429, 296);// x: 250, width: 430 (251, 104, 429, 296)
		songCollectionLable.setBounds(250, 81, 430, 24);
		songCollectionLableShadow.setBounds(250, 80, 430, 24);
		
		// Add
		this.add(songCollectionLable);
		this.add(songCollectionLableShadow);
		this.add(songsCollectionTableScrollPane);
		
		
		
		/* Level 4 */
		/* Playlist List */
		playlistList = new JList<Song>(jukebox.getPlaylist());
		JLabel playlistLable = new JLabel("PLAYLIST");
		JLabel playlistLableShadow = new JLabel("PLAYLIST");
		
		// Setup Style
		playlistLable.setForeground (Color.WHITE);
		playlistLable.setHorizontalAlignment(SwingConstants.CENTER);
		playlistLable.setFont( new Font("Helvetica", Font.PLAIN, 10) );
		playlistLableShadow.setHorizontalAlignment(SwingConstants.CENTER);
		playlistLableShadow.setFont( new Font("Helvetica", Font.PLAIN, 10) );
		playlistList.setFont( new Font("Helvetica", Font.PLAIN, 12) );
		playlistList.setForeground (Color.BLACK);
		playlistList.setFixedCellHeight( 20 );
		playlistList.setFocusable(false);
		
		// Setup Listener (make unselectable)
		playlistList.addListSelectionListener( new setUnselectable());
		
		// Location & Size Setup
		playlistList.setBounds(0, 104, 250, 296);
		playlistLable.setBounds(0, 81, 250, 24);
		playlistLableShadow.setBounds(0, 80, 250, 24);
		
		// Add
		this.add(playlistLable);
		this.add(playlistLableShadow);
		this.add(playlistList);
		
		
		
		
		
		/* Level 5 */
		/* Add to playlist button and theme button */
		addButton = new JButton("Add to Playlist");
		themeButton = new JButton();
		themeButton.setIcon(new ImageIcon("source/theme.png"));
		isClassic = false;
		
		// Setup Style
		themeButton.setBorder(BorderFactory.createEmptyBorder());
		themeButton.setContentAreaFilled(false);
		
		// Setup
		addButton.setEnabled(false);
		
		// Setup actionListener
		addButton.addActionListener(new addListener());
		themeButton.addActionListener(new changeTheme());
		
		// Location & Size Setup
		addButton.setBounds(550, 427, 116, 24);
		themeButton.setBounds(510, 426, 25, 25);	// 15
		
		// Add
		this.add(addButton);
		this.add(themeButton);
		
		
		
		/* Level 6 */
		/* User panel */
		username = new JLabel("");
		songsLeft = new JLabel("");
		timeLeft = new JLabel("");
		nameLabel = new JLabel("Welcome to Jukebox!");
		songsLeftLabel = new JLabel("Please log in first!");
		timeLeftLabel = new JLabel(versionInfo);
		
		usernameShadow = new JLabel("");
		songsLeftShadow = new JLabel("");
		timeLeftShadow = new JLabel("");
		nameLabelShadow = new JLabel("Welcome to Jukebox!");
		songsLeftLabelShadow = new JLabel("Please log in first!");
		timeLeftLabelShadow = new JLabel(versionInfo);
		
		// Setup Style
		Font labelFont = new Font("Helvetica", Font.BOLD, 10);
		nameLabel.setForeground (Color.WHITE);
		songsLeftLabel.setForeground (Color.WHITE);
		timeLeftLabel.setForeground (Color.WHITE);
		nameLabel.setFont( labelFont );
		songsLeftLabel.setFont( labelFont );
		timeLeftLabel.setFont( labelFont );
		Font contentFont = new Font("Helvetica", Font.PLAIN, 10);
		username.setForeground (Color.WHITE);
		songsLeft.setForeground (Color.WHITE);
		timeLeft.setForeground (Color.WHITE);
		username.setFont( contentFont );
		songsLeft.setFont( contentFont );
		timeLeft.setFont( contentFont );
		
		nameLabelShadow.setFont( labelFont );
		songsLeftLabelShadow.setFont( labelFont );
		timeLeftLabelShadow.setFont( labelFont );
		usernameShadow.setFont( contentFont );
		songsLeftShadow.setFont( contentFont );
		timeLeftShadow.setFont( contentFont );
		
		// Location & Size Setup
		nameLabel.setBounds(10, 407, 340, 24);
		songsLeftLabel.setBounds(10, 427, 340, 24);
		timeLeftLabel.setBounds(10, 447, 340, 24);
		username.setBounds(50, 407, 340, 24);
		songsLeft.setBounds(102, 427, 340, 24);
		timeLeft.setBounds(75, 447, 340, 24);
		
		nameLabelShadow.setBounds(10, 406, 340, 24);
		songsLeftLabelShadow.setBounds(10, 426, 340, 24);
		timeLeftLabelShadow.setBounds(10, 446, 340, 24);
		usernameShadow.setBounds(50, 406, 340, 24);
		songsLeftShadow.setBounds(102, 426, 340, 24);
		timeLeftShadow.setBounds(75, 446, 340, 24);
		
		// Add
		this.add(nameLabel);
		this.add(songsLeftLabel);
		this.add(timeLeftLabel);
		this.add(username);
		this.add(songsLeft);
		this.add(timeLeft);
		this.add(nameLabelShadow);
		this.add(songsLeftLabelShadow);
		this.add(timeLeftLabelShadow);
		this.add(usernameShadow);
		this.add(songsLeftShadow);
		this.add(timeLeftShadow);
		
		
		/* Bottom Level */
		/* Set Backgorund*/
		background = new JLabel(new ImageIcon("source/background_blur.png"));
		
		// Location & Size Setup
		background.setBounds(0, 0, 680, 500);

		// Add
		this.add(background);
		
		
		
		/* UIView Settings*/
		this.pack();
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setTitle("Jukebox");
		this.setSize(680, 500);
		this.setResizable(false);
		this.setVisible(true);
		
	}
	
	
	private class changeTheme implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{
			if(isClassic)
			{
				background.setIcon(new ImageIcon("source/background_blur.png"));
				themeButton.setIcon(new ImageIcon("source/theme.png"));
				songsCollectionTable.setSelectionBackground(new Color(0x7997BC));
			
				isClassic = false;	
			}
			else
			{
				background.setIcon(new ImageIcon("source/background.png"));
				themeButton.setIcon(new ImageIcon("source/theme_white.png"));
				songsCollectionTable.setSelectionBackground(new Color(0x3B5392));
			
				isClassic = true;
			}
		}
	}
	
	
	private class signInAndOutListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{
			if(signedIn)
			{
				// LOG OUT
				permissionDenieded();
				
				signedIn = false;
				//idField.setText("");
				passwordField.setText("");
				idField.setEditable(true);
				passwordField.setEditable(true);
				idField.setBackground(Color.WHITE);
				passwordField.setBackground(Color.WHITE);
				signInButton.setText("Log In");
				jukebox.logout();
				
				//idField.addActionListener(this);
				//passwordField.addActionListener(this);
				
			}
			else
			{
				// LOG IN
				boolean logInSuccess = jukebox.login(idField.getText(), String.copyValueOf(passwordField.getPassword()));
				if(logInSuccess)
				{
					permissionApproveded();
					
					signedIn = true;
					passwordField.setText("************");
					idField.setEditable(false);
					passwordField.setEditable(false);
					idField.setBackground(Color.GRAY);
					passwordField.setBackground(Color.GRAY);
					signInButton.setText("Log Out");
					
					//idField.removeActionListener(this);
					//passwordField.removeActionListener(this);
					
				}
				else
				{
					JOptionPane.showMessageDialog(null,
							"Please re-enter your password\nThe username or password you entered is incorrect.",
							"Login Jukebox",
							JOptionPane.INFORMATION_MESSAGE,
							logInIcon);
				}
			}
		}

		// Load up info for log in
		private void permissionApproveded()
		{
			addButton.setEnabled(true);
			nameLabel.setText("NAME:");
			songsLeftLabel.setText("SONGS REMAIN:");
			timeLeftLabel.setText("TIME LEFT:");
			
			nameLabelShadow.setText("NAME:");
			songsLeftLabelShadow.setText("SONGS REMAIN:");
			timeLeftLabelShadow.setText("TIME LEFT:");
			
			updateUserDetail();
		}
		

		// Called when log out
		private void permissionDenieded()
		{
			addButton.setEnabled(false); 
			
			username.setText("");
			songsLeft.setText("");
			timeLeft.setText("");
			nameLabel.setText("Welcome to Jukebox!");
			songsLeftLabel.setText("Please log in first!");
			timeLeftLabel.setText(versionInfo);
			
			usernameShadow.setText("");
			songsLeftShadow.setText("");
			timeLeftShadow.setText("");
			nameLabelShadow.setText("Welcome to Jukebox!");
			songsLeftLabelShadow.setText("Please log in first!");
			timeLeftLabelShadow.setText(versionInfo);
		}
	}
	
		
	private void updateUserDetail()
	{
		int timeLefted = jukebox.getCurrentUser().getTimeLeft(jukebox.getMaxPlayTime());
		String timeFormated = "00:00:00";
		DecimalFormat twoDig = new DecimalFormat("#00.###");  
		twoDig.setDecimalSeparatorAlwaysShown(false);  
		timeFormated = twoDig.format(timeLefted / 3600) + ":" + twoDig.format((timeLefted%3600) / 60) + ":" + twoDig.format((timeLefted%3600) % 60);
		
		username.setText(jukebox.getCurrentUser().getName());
		songsLeft.setText(jukebox.getCurrentUser().getSongsLeft());
		timeLeft.setText(timeFormated);
		
		usernameShadow.setText(jukebox.getCurrentUser().getName());
		songsLeftShadow.setText(jukebox.getCurrentUser().getSongsLeft());
		timeLeftShadow.setText(timeFormated);
	}
	

	
	private class addListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			
			int viewRow = songsCollectionTable.getSelectedRow();
			if(arg0.getSource() == addButton)
			{
				if(viewRow < 0)
				{
					JOptionPane.showMessageDialog(null, "No song selected!");
				}
				else
				{
					int modelRow = songsCollectionTable.convertRowIndexToModel(viewRow);
					Song selectedSong = ((SongCollection) songsCollectionTableModel).getList().get(modelRow);
					if(jukebox.getCurrentUser().canPlay(selectedSong, jukebox.getMaxPlayTime()))
					{
		
						jukebox.playSong(selectedSong);
						
						selectedSong.increaseCount();
						jukebox.getCurrentUser().increaseCountAndSecsPlayed(selectedSong.getSongLengthSec());
						
						
					
					 
						updateUserDetail();
					}
					else
					{
						JOptionPane.showMessageDialog(null,
								"You may not play this song.",
								"Alert!",
								JOptionPane.INFORMATION_MESSAGE,
								alertIcon);
					}
					
				}
				
				// update even when selection
				songsCollectionTable.updateUI();
			}
		}



	}
	
	private class setUnselectable implements ListSelectionListener
	{

		@Override
		public void valueChanged(ListSelectionEvent arg0)
		{
			playlistList.clearSelection();
		}

	}
	
	
}
