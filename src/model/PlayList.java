package model;

/**
 * @author Collin Gifford
 * @author YUNHAO ZHANG
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.LinkedList;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import songplayer.EndOfSongEvent;
import songplayer.EndOfSongListener;
import songplayer.SongPlayer;

/*
 * Represents a self-managing playlist of songs which causes songs to be audibly played in the order requested, as each finishes
 */

public class PlayList implements EndOfSongListener, ListModel<Song>
{

	private Queue<Song> playlist;
	private boolean songIsPlaying;

	private List<ListDataListener> listeners;

	public PlayList()
	{
		this.songIsPlaying = false;
		this.playlist = new LinkedList<Song>();

		this.listeners = new ArrayList<ListDataListener>();
	}

	@Override
	public void songFinishedPlaying(EndOfSongEvent eventWithFileNameAndDateFinished)
	{
		playlist.remove();
		if(!playlist.isEmpty())
		{
			long currentTime = System.currentTimeMillis();
			while (System.currentTimeMillis() < currentTime + 1000)
			{
				continue;
			}
			SongPlayer.playFile(this, playlist.peek().getFileName());
		}
		if(playlist.isEmpty())
		{
			songIsPlaying = false;
		}

		for (ListDataListener listener : listeners)
		{
			listener.intervalRemoved(new ListDataEvent(this,
					ListDataEvent.INTERVAL_REMOVED, 0, 0));
		}
	}

	public void queueUpNextSong(Song song)
	{
		if(!songIsPlaying)
		{
			songIsPlaying = true;
			playlist.add(song);
			SongPlayer.playFile(this, song.getFileName());

		}
		else
		{
			playlist.add(song);
		}

		for(ListDataListener listener : listeners)
		{
			listener.intervalAdded(new ListDataEvent(this,
					ListDataEvent.INTERVAL_ADDED, this.playlist.size() - 1,
					this.playlist.size() - 1));
		}
	}

	public List<Song> getPlaylistList()
	{
		return new LinkedList<Song>(playlist);
	}

	@Override
	public void addListDataListener(ListDataListener arg0)
	{
		this.listeners.add(arg0);

	}

	@Override
	public Song getElementAt(int arg0)
	{
		return ((LinkedList<Song>) playlist).get(arg0);
	}

	@Override
	public int getSize()
	{

		return this.playlist.size();
	}

	@Override
	public void removeListDataListener(ListDataListener arg0)
	{
		// Operation not supported; don't use this method, Hercy
		throw new UnsupportedOperationException();

	}
	

	public String toString()
	{
		String temp = "";
		for(int i = 0; i < playlist.size(); i++)
		{
			temp = temp + " - " + ((LinkedList<Song>) playlist).get(i).getFileName();
			
		}
		return temp;
	}
}
